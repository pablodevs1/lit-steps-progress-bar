import { html } from 'lit';
import '../genk-step-progress-bar.js';

export default {
  title: 'GenkStepProgressBar',
  component: 'genk-step-progress-bar',
  argTypes: {
    steps: { control: 'array' },
    activeStep: { control: 'number', min: 1 },
  },
};

function Template ({ steps, activeStep = 1 }) {
  return html`
    <genk-step-progress-bar .steps=${steps} activeStep="${activeStep}"></genk-step-progress-bar>
  `;
}

export const CustomSteps = Template.bind({});
CustomSteps.args = {
  steps: [
    {
      badge: 'A',
      label: 'Primero'
    },
    {
      badge: 'B',
      label: 'Segundo'
    },
    {
      badge: 'C',
      label: 'Tercero'
    },
    {
      badge: 'D',
      label: 'Cuarto'
    }
  ]
};

