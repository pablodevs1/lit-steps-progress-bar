import { expect, fixture } from '@open-wc/testing';
import { html } from 'lit';
import '../genk-step-progress-bar.js';

const STEPS = [
  {
    badge: 'A',
    label: 'Primero'
  },
  {
    badge: 'B',
    label: 'Segundo'
  },
  {
    badge: 'C',
    label: 'Tercero'
  },
  {
    badge: 'D',
    label: 'Cuarto'
  }
];

describe('GenkStepProgressBar', () => {
  it('receives steps correctly', async () => {
    const el = await fixture(html`<genk-step-progress-bar .steps="${ STEPS }"></genk-step-progress-bar>`);

    expect(el.steps).to.have.lengthOf(4);
    expect(el.activeStep).to.equal(1);
  });

  it('passes the a11y audit', async () => {
    const el = await fixture(html`<genk-step-progress-bar .steps="${ STEPS }" activeStep="4"></genk-step-progress-bar>`);

    await expect(el).to.be.accessible();
    await expect(el).shadowDom.to.be.accessible();
  });
});
