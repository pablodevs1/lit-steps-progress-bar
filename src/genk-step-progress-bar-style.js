import { css } from 'lit';

export const defaultStyles = css`
  :host {
    display: block;
  }
  ol, li {
    list-style: none;
    padding: 0;
    margin: 0;
  }
  .list-steps {
    --clr-primary: var(--genk-spb-clr-primary, hsl(28, 100%, 50%));
    --clr-secondary: var(--genk-spb-clr-secondary, hsl(232, 100%, 50%));
    --clr-dark: var(--genk-spb-clr-dark, hsl(0, 0%, 30%));
    --clr-grey: var(--genk-spb-clr-grey, hsl(0, 0%, 85%));
    --clr-light: var(--genk-spb-clr-light, hsl(0, 0%, 98%));

    --bar-width: var(--genk-spb-width, 450px);
    --badge-size: var(--genk-spb-badge-size, 35px);

    --transition-duration: var(--genk-spb-transition-duration, 400ms);
    --transition-delay: var(--genk-spb-transition-delay, 0s);
    --transition-timing-function: var(--genk-spb-transition-timing-function, ease);
    --transition: all var(--transition-duration) var(--transition-timing-function) var(--transition-delay);
    
    z-index: 0;
    position: relative;
    display: flex;
    justify-content: space-between;
    width: var(--bar-width);
  }
  .list-steps::before {
    content: '';
    z-index: -2;
    position: absolute;
    left: 0;
    top: calc(var(--badge-size) / 2);
    width: 100%;
    height: calc(var(--badge-size) / 10);
    transform: translateY(-50%);

    background-color: var(--clr-grey);
  }

  .step-item {
    display: flex;
    flex-direction: column;
    align-items: center;
    gap: 0.25em;
    max-width: var(--badge-size);
    color: var(--clr-dark);
  }
  .step-item.completed {
    color: var(--clr-primary);
  }
  .step-item.active > .badge {
    color: var(--clr-light);
    background-color: var(--clr-secondary);
  }
  .step-item.completed > .badge {
    color: var(--clr-light);
    background-color: var(--clr-primary);
  }

  .step-item.active {
    color: var(--clr-secondary);
  }

  .badge, .label {
    transition: var(--transition);
  }

  .label {
    font-size: 0.85em;
  }
  .badge {
    position: relative;
    display: grid;
    place-items: center;
    font-size: 1em;
    font-weight: 600;
    width: var(--badge-size);
    aspect-ratio: 1;
    border-radius: 50%;
    background-color: var(--clr-grey);
  }

  .progress {
    z-index: -1;
    position: absolute;
    left: 0;
    top: calc(var(--badge-size) / 2);
    width: 0%;
    height: calc(var(--badge-size) / 10);
    transform: translateY(-50%);
    background-color: var(--clr-primary);

    transition: var(--transition);
  }
`;