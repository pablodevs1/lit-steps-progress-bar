import { html, LitElement } from 'lit';
import { classMap } from 'lit-html/directives/class-map.js';
import { defaultStyles } from './genk-step-progress-bar-style.js';

/**
 * `genk-step-progress-bar`
 * GenkStepProgressBar
 *
 * @customElement genk-step-progress-bar
 * @litElement
 * @demo demo/index.html
 */
export class GenkStepProgressBar extends LitElement {
  static get styles() {
    return [defaultStyles];
  }

  static get properties() {
    return {
      /**
       * Optional Array of Objects with Badge and/or Label key-values
       * @property
       * @type { Array }
       */
      steps: { type: Array },
      /**
       * Initial active step, from 1 to steps.length
       * @property
       * @type { Number }
       */
      activeStep: { type: Number },
    };
  }

  async attributeChangedCallback(name, oldval, newval) {
    await this.updateComplete;
    this._customUpdated(newval);
    // eslint-disable-next-line wc/guard-super-call
    super.attributeChangedCallback(name, oldval, newval);
  }

  _customUpdated(step) {
    const progressBar = this.shadowRoot.querySelector("#progress");
    progressBar.style.width = `${ ((step - 2) / (this.steps.length - 1)) * 100 }%`;
  }

  render() {
    let classes = { completed: false, active: false };
    const steps = this.steps.map(({ badge, label }, index) => {
      classes = { completed: index < this.activeStep - 1, active: index === this.activeStep - 1 };
      return html`
        <li part="step-item" class="step-item ${ classMap(classes) }">
          <span part="badge" class="badge">${ badge || index + 1 }</span>
          ${ label ? html`
          <span part="label" class="label">${ label || '' }</span>
          ` : '' }
        </li>
      `;
    });

    return html`
      <ol class="list-steps" part="list-steps">
        <li class="progress" id="progress"></li>
        ${ steps }
      </ol>
    `;
  }
}
