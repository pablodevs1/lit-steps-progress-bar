import { css, html, LitElement } from 'lit';
import '../genk-step-progress-bar.js';

const STEPS = [
  {
    badge: 'A',
    label: 'First'
  },
  {
    badge: 'B',
    label: 'Second'
  },
  {
    badge: 'C',
    label: 'Third'
  },
  {
    badge: 'D',
    label: 'Fourth'
  }
];

export class GenkDemo extends LitElement {

  static get properties() {
    return {
      stepNumber: { type: Number },
    };
  }

  static get styles() {
    return css`
      :host {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);

        font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;

        /*===== Customize with CSS variables =====*/
        --genk-spb-badge-size: 50px;
        --genk-spb-transition-duration: 1s;
      }
      
      /*===== Customize with ::part(step-item, badge, label, list-steps) pseudo-elements =====*/
      genk-step-progress-bar::part(label) {
        font-size: 1.1em;
        margin-top: 0.5em;
      }

      genk-step-progress-bar::part(badge) {
        font-size: 1.3em;
      }

      /*===== Next and Prev Buttons =====*/
      .wrapper {
        display: flex;
        justify-content: center;
        gap: 1rem;
        margin-top: 4rem;
      }

      .wrapper > button {
        cursor: pointer;
        font-size: 1.1rem;
        padding: 1rem 2rem;
        background-color: hsl(0, 0%, 90%);
        border: none;
        border-radius: 5px;

        transition: all 200ms ease;
      }

      .wrapper > button:hover {
        background-color: hsl(0, 0%, 80%);
      }

      .wrapper > button:active {
        transform: scale(0.9);
      }

      genk-step-progress-bar::part(badge) {
        line-height: 1;
      }
    `;
  }

  constructor() {
    super();

    this.stepNumber = 1;
  }

  goToNextStep() {
    if (this.stepNumber <= STEPS.length)
      this.stepNumber += 1;
  }

  goToPrevStep() {
    if (this.stepNumber > 1)
      this.stepNumber -= 1;
  }

  render() {
    return html`
      <genk-step-progress-bar .steps="${ STEPS }" activeStep="${ this.stepNumber }"></genk-step-progress-bar>
      <div class="wrapper">
        <button @click="${ this.goToPrevStep }">PREV</button>
        <button @click="${ this.goToNextStep }">NEXT</button>
      </div>
    `;
  }

}

window.customElements.define('genk-demo', GenkDemo);