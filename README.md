<div align="center">
<h1 align="center">genk-step-progress-bar</h1>
  <p align="center">
    <img alt="Step Progress Bar" src="https://gitlab.com/pablodevs1/lit-steps-progress-bar/-/raw/master/progress-bar-gif.gif" width="500" />
</p>
</div>

Multi step and dynamic progress bar built with Lit-Element. This webcomponent follows the [open-wc](https://github.com/open-wc/open-wc) recommendation.

<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#installation">Installation</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#usage">Usage</a>
      <ul>
        <li><a href="#available-props">Available Props</a></li>
        <li><a href="#available-css-variables">Available CSS variables</a></li>
        <li><a href="#events">Events</a></li>
      </ul>
    </li>
    <li>
      <a href="#linting">Linting</a>
    </li>
    <li>
      <a href="#testing">Testing</a>
    </li>
    <li>
      <a href="#tooling-configs">Tooling configs</a>
    </li>
    <li>
      <a href="#local-demo">Local Demo</a>
    </li>
    <li>
      <a href="#contact">Contact</a>
    </li>
  </ol>
</details>

## Installation

```bash
npm install genk-step-progress-bar --save
```

### Built With

* [Node.js](https://nodejs.org/en/) (v18.0.0)
* [Lit](https://lit.dev/)

## Usage

```html
<script type="module">
  import 'genk-step-progress-bar/genk-step-progress-bar.js';
</script>

<genk-step-progress-bar .steps=${ARRAY}></genk-step-progress-bar>
```

### Available Props

#### Required

- steps (`Array`) - Array of Objects with Badge and/or Label key-values

#### Optional

- activeStep (`Number`) - Initial active step, 1 by default


### Available CSS variables

#### Colors
```css
--genk-spb-clr-primary
--genk-spb-clr-secondary
--genk-spb-clr-dark
--genk-spb-clr-grey
--genk-spb-clr-light
```

#### Dimensions
```css
--genk-spb-width
--genk-spb-badge-size
```

#### Transitions
```css
--genk-spb-transition-duration
--genk-spb-transition-delay
--genk-spb-transition-timing-function
```

## Linting

To scan the project for linting and formatting errors, run

```bash
npm run lint
```

To automatically fix linting and formatting errors, run

```bash
npm run format
```

## Testing

To execute a single test run:

```bash
npm run test
```

To run the tests in interactive watch mode run:

```bash
npm run test:watch
```


## Tooling configs

For most of the tools, the configuration is in the `package.json` to minimize the amount of files in your project.

If you customize the configuration a lot, you can consider moving them to individual files.

## Local Demo

```bash
npm run start
```

To run a local development server with `web-dev-server` that serves the basic demo located in `demo/index.html`

## Contact

Pablo Álamo - [LinkedIn](https://www.linkedin.com/in/pablodevs/) - pabloalamovargas@gmail.com

Project Link: [https://gitlab.com/pablodevs1/lit-steps-progress-bar](https://gitlab.com/pablodevs1/lit-steps-progress-bar)

<div align="right">
  <p>(<a href="#top">back to top</a>)</p>
</div>
